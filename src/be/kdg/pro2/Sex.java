package be.kdg.pro2;

public enum Sex {
	M("male"),
	F("female"),
	X("fluid");
	private String text;

	Sex(String text) {
		this.text = text;
	}
}
