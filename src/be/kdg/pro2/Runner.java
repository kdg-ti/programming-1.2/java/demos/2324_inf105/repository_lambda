package be.kdg.pro2;

import java.util.function.Predicate;

import static be.kdg.pro2.Sex.*;

public class Runner {

	public static void main(String[] args) {
		Runner runner = new Runner();
		runner.query();
	}

	private  void query() {
		PersonRepository database = new PersonRepository();
		database.add(new Person("Angele",27, F ));
		database.add(new Person("Romeo Elvis",30,M));
		database.add(new Person("Marka",60,X ));
		database.add(new Person("Lisette",1,F ));
		//System.out.println(database.getBy(new AdultTester()));
		System.out.println(database.getBy(person -> person.getAge() >= 18));
		System.out.println(database.getBy(person -> person.getSex() == M));
		System.out.println(database.getBy(person -> person.getSex() == F && person.getAge()>=18));
	}


//	private  class AdultTester implements Predicate<Person> {
//		@Override
//		public boolean test(Person person) {
//			return person.getAge() >= 18;
//		}
//	}
}
